# canvas video test

Two videos, side-by-side, fighting over the same canvas. One version, on [the master branch](https://gitlab.com/Rich-Harris/walking-in-memphis), for iPhone (which requires some jiggery-pokery to avoid the video taking over the screen) and one on [the non-iphone branch](https://gitlab.com/Rich-Harris/walking-in-memphis/tree/non-iphone) for everyone else.
