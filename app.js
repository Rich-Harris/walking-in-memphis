var canvas = document.querySelector( 'canvas' );
var ctx = canvas.getContext( '2d' );

var left = document.querySelector( '#left' );
var right = document.querySelector( '#right' );

left.load();
right.load();

var leftAudio = document.createElement( 'audio' );
leftAudio.innerHTML = left.innerHTML;

var rightAudio = document.createElement( 'audio' );
rightAudio.innerHTML = right.innerHTML;

leftAudio.volume = rightAudio.volume = 0.5;

var WIDTH = 480;
var HEIGHT = 270;

canvas.width = WIDTH;
canvas.height = HEIGHT;

var playing = false;
var x = WIDTH / 2;

function toggle () {
	if ( playing ) {
		leftAudio.pause();
		rightAudio.pause();

		playing = false;
	}

	else {
		leftAudio.play();
		rightAudio.play();

		playing = true;
		loop();
	}
}

canvas.addEventListener( 'click', toggle );

function setX ( clientX ) {
	var bcr = canvas.getBoundingClientRect();
	var pos = ( clientX - bcr.left ) / ( bcr.right - bcr.left );

	leftAudio.volume = pos;
	rightAudio.volume = 1 - pos;
	x = canvas.width * pos;
}

canvas.addEventListener( 'mousemove', function ( event ) {
	setX( event.clientX );
});

canvas.addEventListener( 'touchstart', function ( event ) {
	function handleTouchmove ( event ) {
		event.preventDefault();
		setX( event.touches[0].clientX );
	}

	function cancel () {
		window.removeEventListener( 'touchmove', handleTouchmove );
		window.removeEventListener( 'touchend', cancel );
	}

	window.addEventListener( 'touchmove', handleTouchmove );
	window.addEventListener( 'touchend', cancel );
});

function loop () {
	if ( !playing ) return;

	requestAnimationFrame( loop );

	left.currentTime = leftAudio.currentTime;
	right.currentTime = rightAudio.currentTime;

	ctx.drawImage( left, 0, 0, x, HEIGHT, 0, 0, x, HEIGHT );
	ctx.drawImage( right, x, 0, WIDTH - x, HEIGHT, x, 0, WIDTH - x, HEIGHT );

	ctx.fillStyle = 'white';
	ctx.fillRect( x - 1, 0, 2, HEIGHT );
}
